## Qu'est-ce que le taux de quantification ?

<img src="pesticides_RShiny_accueil_quantification.png" alt="Taux de quantification en 2016" class="apropos"/>

Le **taux de quantification** est le résultat de la division du nombre d'occurrences où la molécule est détectée par le nombre d'occurrences où la molécule est recherchée.

Le taux de quantification peut être calculé sur n'importe quelle période, annuelle ou pluri-annuelle par exemple. Il peut être calculé pour l'ensemble de la région, pour un SAGE donné ou pour une station déterminée.

Ce taux a pour utilité de classer les molécules en fonction de leur fréquence d'apparition pendant une période donnée. Cette approche permet de voir les molécules présentes sur une période, celles dont les apparitions sont plus rares avec le temps et au contraire celles qui émergent.

La **fréquence de dépassement** est le résultat de la division du nombre d'occurrences où la molécule est détectée et sa concentration est supérieure ou égale au seuil de 0,1 μg/l, par le nombre d'occurrences où la molécule est recherchée.
