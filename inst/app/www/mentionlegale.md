# 

## Service gestionnaire

Direction Régionale de l'Environnement de l'Aménagement et du Logement des Pays de la Loire

5, rue Françoise Giroud

CS 16326

44263 NANTES Cedex 2

Tél : 02 72 74 73 00

Fax : 02 72 74 73 09

Courriel : [dreal-paysdelaloire\@developpement-durable.gouv.fr](mailto:dreal-paysdelaloire@developpement-durable.gouv.fr)

## Directrice de publication

Anne Beauval, directrice régionale de l'environnement, de l'aménagement et du logement des Pays de la Loire.

## Conception, Réalisation

-   Charte graphique, ergonomie : DREAL Pays de la Loire

-   Développement : Franck GASPARD, DREAL Pays de la Loire

## Hébergement

-   Rstudio - plateforme Shinyapps - <http://shinyapps.io/>

## Droit d'auteur - Licence

Tous les contenus présents sur le site de la Direction Régionale de l'Environnement, de l'Aménagement et du Logement des Pays de la Loire sont couverts par le droit d'auteur. Toute reprise est dès lors conditionnée à l'accord de l'auteur en vertu de l'article L.122-4 du Code de la Propriété Intellectuelle.

Toutes les informations liées à cette application (données et textes) sont publiées sous [licence ouverte/open licence v2](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf) (dite licence Etalab) : quiconque est libre de réutiliser ces informations, sous réserve notamment, d'en mentionner la filiation.

Tous les scripts source de l'application sont disponibles sous [licence GPL-v3](https://spdx.org/licenses/GPL-3.0.html#licenseText).

## Code source

L'ensemble des scripts de collecte et de datavisualisation est disponible sur le [répertoire gitlab du DREAL datalab](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/pesticides.pdl). Vous pouvez y reporter les éventuels bugs ou demandes d'évolution au niveau de la rubrique Issue.

## Établir un lien

-   Tout site public ou privé est autorisé à établir, sans autorisation préalable, un lien vers les informations diffusées par le Ministère de la Transition Ecologique et le Ministère de la Cohésion des Territoires.

-   L'autorisation de mise en place d'un lien est valable pour tout support, à l'exception de ceux diffusant des informations à caractère polémique, pornographique, xénophobe ou pouvant, dans une plus large mesure porter atteinte à la sensibilité du plus grand nombre.

-   Pour ce faire, et toujours dans le respect des droits de leurs auteurs, une icône "Marianne" est disponible [ici](http://www.pays-de-la-loire.developpement-durable.gouv.fr/local/cache-vignettes/L105xH136/arton1255-30208.jpg) pour agrémenter votre lien et préciser que le site d'origine est celui du Ministère de la Transition Ecologique ou du Ministère de la Cohésion des Territoires.

## Usage

-   Les utilisateurs sont responsables des interrogations qu'ils formulent ainsi que de l'interprétation et de l'utilisation qu'ils font des résultats. Il leur appartient d'en faire un usage conforme aux réglementations en vigueur et aux recommandations de la CNIL lorsque des données ont un caractère nominatif (loi n° 78.17 du 6 janvier 1978, relative à l'informatique, aux fichiers et aux libertés dite loi informatique et libertés).

-   Il appartient à l'utilisateur de ce site de prendre toutes les mesures appropriées de façon à protéger ses propres données et/ou logiciels de la contamination par d'éventuels virus circulant sur le réseau Internet. De manière générale, la Direction Régionale de l'Environnement de l'Aménagement et du Logement des Pays de la Loire décline toute responsabilité à un éventuel dommage survenu pendant la consultation du présent site. Les messages que vous pouvez nous adresser transitant par un réseau ouvert de télécommunications, nous ne pouvons assurer leur confidentialité.
