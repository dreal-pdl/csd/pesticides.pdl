## La classification des prélèvements pour l'eau potable

<img src="pesticides_RShiny_accueil_cumul-prelevement.png" alt="Les cumuls de pesticides par prélèvement" class="apropos"/>

Pour chaque prélèvement validé (une station, une date), les concentrations des différentes molécules peuvent être additionnées afin de déterminer la concentration totale. Il est alors question de **cumul par prélèvement**.

La **classification des prélèvements** utilise deux types de seuils de concentration :

-   une limite à partir de laquelle l'eau brute (prélevée dans le milieu) doit être traitée avant d'être distribuée :

    -   0,1 μg/l dépassé pour au moins une molécule ;
    -   0,5 μg/l dépassé pour le cumul.

-   une limite à partir de laquelle cette eau ne peut être utilisée pour produire de l'eau potable :

    -   2 μg/l dépassé pour au moins une molécule ;
    -   5 μg/l dépassé pour le cumul.
