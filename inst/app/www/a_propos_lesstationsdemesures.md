## Les stations de mesures

Les stations de mesures des pesticides où sont effectués les prélèvements appartiennent à deux réseaux :

1.  <b>le réseau de suivi de l'Agence Régionale de Santé</b>, destiné principalement au contrôle des teneurs en pesticides vis-à-vis de la production d'eau potable ;

2.  <b>le réseau de suivi de l'Agence de l'Eau Loire Bretagne</b>, destiné à la connaissance générale de l'état des cours d'eau.


<img src="Vaudelle_53.jpg" alt="Vaudelle_53.jpg" width="50%">
