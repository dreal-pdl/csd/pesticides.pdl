Cette application met à disposition les données sur les pesticides dans les cours d'eau de la région des Pays de la Loire à partir de 2011 et propose des indicateurs pour visualiser leur évolution et leur répartition. Elle met également à disposition les données des nappes souterraines de la région.

Ces données sont issues de l'Agence de l'Eau Loire Bretagne, de l'Agence Régionale de Santé.

Elles étaient complétées par celles d'un réseau complémentaire de suivi régional qui était financé par la DREAL, la DRAAF et l'Agence de l'Eau. Le suivi de ce réseau a été suspendu fin 2019.

Ces données seront mises à jour régulièrement et l'application enrichie au fur et à mesure.

N'hésitez pas à [nous contacter](mailto:srnp.dreal-paysdelaloire@developpement-durable.gouv.fr?subject=Application%20pesticides&body=Bonjour,%22) pour toute remarque ou suggestion.
