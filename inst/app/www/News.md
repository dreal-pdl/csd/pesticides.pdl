# 

Annonce des évolutions de l'application et des données qu'elle valorise.

## Pesticides 2.2

Publiée le 12 février 2024

### Evolution des données

- Ajout des données des eaux souterraines

- Ajout des données 2021 et 2022

### Evolutions de l'application

- L'application utilise  le [Système de Design de l'Etat (DSFR)](https://www.systeme-de-design.gouv.fr/). Le codage s'appuie sur le package [{shinygouv}](https://github.com/spyrales/shinygouv).

- La page "Explorer les mesures" intègre les mesures relatives aux nappes d'eau souterraine.

- Un nouvelle page "Eaux souterraines" est en cours de conception. A l'instar de la page "Eaux superficielles", elle présentera un tableau de bord permettant de visualiser des indicateurs.


## Pesticides 2.1.1

Publiée le 17 mai 2022

### Evolutions de l'application

#### Page Tableau de bord

-   Amélioration de la performance de la page
-   Reconfiguration des illustrations de quantification et recherche de molécules
-   Message d'avertissement vis à vis de l'onglet de Quantification par station
-   Mention de la suspension du suivi du réseau de mesures complémentaires fin 2019

## Pesticides 2.1

Publiée le 17 mars 2022

### Evolution des données

-   Ajout des données 2019 et 2020

## Pesticides 2.0

Publiée le 28 juillet 2020

### Evolution des données

-   Ajout des données 2018
-   Chargement des données brutes à partir de l'API "Qualité des cours d'eau" de [la plateforme Hub'eau](https://hubeau.eaufrance.fr/page/api-qualite-cours-deau)

### Evolutions de l'application

#### Page Tableau de bord

-   Ajout d'un diagramme "répartition des classes de qualité par année"
-   Ajout d'une carte des taux de quantification par station en fonction de la molécule sélectionnée parmi les 15 les plus trouvées
-   Ajout d'un tableau des stations de la carte de quantification, mentionnant pour la molécule sélectionnée : taux de quantification, fréquence de dépassement, nombre de fois où elle a été recherchée,trouvée ou non trouvée
-   Ajout d'un bouton de téléchargement (png, jpeg, pdf et csv) aux diagrammes

#### Page explorer les données

-   Ajout d'une carte de localisation des stations issues des sélections
-   Enrichissement du tableau des prélèvements sélectionnés par la mention du code et du département des stations
-   Mise à jour de l'interface

#### A propos / Les classes de qualité

-   Présentation plus pédagogique de la méthode du SeqEau

#### Mentions légales

-   Mention des licences permettant la réutilisation des informations et des scripts source de l'application

## Pesticides 1.2.4

Publiée le 6 mai 2019

### Evolutions de l'application

-   Rajout d'un filtre sur le réseau patrimonial dans la page "explorer les données".
-   Correction de bug sur l'export des données

## Pesticides 1.2

Publiée le 27 mars 2019

### Evolution des données

-   Ajout des données 2017
-   Modifications des données de références historiques pour se baser sur l'ensemble de l'historique des données OSUR et Agence Régionale de Santé
-   Amélioration qualité pour supprimer les mesures réalisées parfois en double pour une même molécule et un même prélèvement

### Evolutions de l'application

-   Amélioration de la performance
-   Rajout d'une page News
-   Rajout de la possibilité d'explorer la table des molécules
-   Amélioration de la navigation de la carte de la page "tableau de bord" :
    -   Possibilité de cliquer sur un sage voisin sur la carte quand on a déjà zoomé sur un SAGE
    -   Rajout d'un bouton "revenir à la région" quand on a zommé sur un SAGE
-   Passage du seuil 0.5 µg/l à 0.1 µg/l quand on ne sélectionne que des molécules et pas le cumul sur la page "Explorer les mesures"
-   Ajout de la possibilité de sélectionner les stations hors SAGE dans les sélecteurs de SAGE.
-   Ajout d'un graphique sur la page "Tableau de bord" pour visualiser le nombre de mesures effectuées pour les 15 molécules les plus retrouvées.

## Pesticides 1.0

Publiée le 8 avril 2018.

Version initiale
