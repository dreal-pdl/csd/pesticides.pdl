# Que sont les classes de qualité des cours d’eau par station ?

<div class = "row">

<div class = "col-md-6">

<p>
La classification de la « qualité » de l’eau par la <b>méthode du SeqEau</b> vise à qualifier l’aptitude de l’eau à permettre le bon fonctionnement des différents compartiments biologiques présents dans le cours d’eau, ainsi que les différents usages liés à la santé.
<p>

Elle repose sur une liste de molécules et de 4 valeurs seuils de concentration propres à chaque molécule ainsi que pour le cumul des concentrations prélevées. Ces 4 seuils délimitent <b>5 classes de qualité</b>, de très bonne à mauvaise. 
<p>

Il n’est utilisé ici que la liste des molécules « pesticides » du SEQ’eau v2 de 2003, dont un extrait est représenté ci-contre.
<p>

Le tableau est accessible dans son intégralité en cliquant <a href="classe_qualite_parametre.xlsx" target="_blank">ici</a>.

<p>
Des classes de qualité sont définies pour 79 molécules relevant des pesticides (substances phyto-pharmaceutiques ou biocides) : pour toutes les autres pesticides, les seuils correspondant à « Pesticides (autres) » sont attribués.

</div>

<div class = "col-md-6">

<b>Extrait du tableau de définition des classes de qualité par molécule</b>

<img style="float:left;margin:5px 20px 5px 0px" src="pesticides_RShiny_extrait_tableau_seuils_molecules.png" width="70%"/>

</div>
</div>


<div class = "row">

<div class = "col-md-6">


<p>
Pour chaque prélèvement réalisé dans l’année et pour chaque station, une classe de qualité (couleur) est attribuée à chaque molécule retrouvée et à la somme des molécules. On attribue alors à chaque prélèvement la classe la plus défavorable.
<p>

Le calcul s’effectue ensuite en fonction du nombre de prélèvements par an et pour chaque station :  
<p>
- <b>cas 1</b> : moins de 4 strictement : on ne calcule pas de SEQ’eau ;  
<p>
- <b>cas 2</b> : entre 4 et 10 prélèvements : on prend la valeur de la classe de qualité la plus défavorable des prélèvements ;  
<p>
- <b>cas 3</b> : au-delà de 10 prélèvements : on prend la valeur du 2ème prélèvement le plus défavorable.
<p>

C’est un principe proche du « percentile 90 » qu’on attribue ici au nombre de prélèvements, qui permet à de retirer les valeurs les plus importantes dès lors qu’on a suffisamment de prélèvements.
<p>
<p>

La <b>carte de la qualité des cours d’eau par station</b> de la page <b>Tableau de bord</b> présente les résultats de chaque station par année.
<p>

Le diagramme en forme de donut de la même page présente la répartition des stations par classe de qualité selon la méthode du SeqEau pour une année donnée.

</div>

<div class = "col-md-6">

<b>Exemple de calcul du SEQ'eau</b>

<img style="float:left;margin:5px 20px 5px 0px" src="pesticides_RShiny_exemples_seqeau.png" width="50%">

</div>
</div>
