#' tableau_concentration_esu
#'
#' concentration maximale et moyenne des molecules les plus quantifiees dans ESU.
#'
#' @format A data frame with 5255 rows and 7 variables:
#' \describe{
  #'   \item{ annee }{  factor }
  #'   \item{ nom_parametre }{  factor }
  #'   \item{ resultat_analyse }{  numeric }
  #'   \item{ moyenne }{  numeric }
  #'   \item{ libelle_station_max }{  factor }
  #'   \item{ nom_sage_max }{  factor }
  #'   \item{ nom_sage }{  factor }
  #' }
  #' @source DREAL
  "tableau_concentration_esu"
