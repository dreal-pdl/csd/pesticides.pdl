#' matrice_esu
#'
#' dataframe recaptulative des prelevements et des annalyses par station ESU.
#'
#' @format A data frame with 8245336 rows and 10 variables:
#' \describe{
  #'   \item{ annee_prelevement }{  factor }
  #'   \item{ date_prelevement }{  Date }
  #'   \item{ nom_sage }{  factor }
  #'   \item{ code_station }{  factor }
  #'   \item{ libelle_station }{  factor }
  #'   \item{ insee_dep }{  factor }
  #'   \item{ nom_parametre }{  factor }
  #'   \item{ code_remarque }{  factor }
  #'   \item{ resultat_analyse }{  numeric }
  #'   \item{ Exigenceeaudistribuee }{  factor }
  #' }
  #' @source DREAL
  "matrice_esu"
