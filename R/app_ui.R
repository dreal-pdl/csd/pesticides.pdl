#' The application User-Interface
#'
#' @param request Internal parameter for `{shiny}`.
#'     DO NOT REMOVE.
#' @import shiny
#' @import shinygouv
#' @noRd
#' @importFrom gouvdown gouv_colors
#' @importFrom shiny includeMarkdown
#' @importFrom shinybusy add_busy_spinner
#' @importFrom shinyWidgets useShinydashboard
app_ui <- function(request) {
  tagList(

    # Leave this function for adding external resources
    golem_add_external_resources(),

    # Your application UI logic
    navbarPage_dsfr(
      id = "nav",
      title = "DREAL des Pays de la Loire", # n'apparait pas !

      header = header_dsfr(
        intitule = c("PREFET", "DE LA REGION", "PAYS DE LA LOIRE"),
        nom_site_service = "Les pesticides dans les cours d\'eau et les nappes souterraines des Pays de la Loire", # en fait le titre du site !
        # url = "https://www.pays-de-la-loire.developpement-durable.gouv.fr/spip.php?page=contact",
        # titre = "Nous contacter"
      ),

      footer = footer_dsfr(
        intitule = c("PREFET", "DE LA REGION", "PAYS DE LA LOIRE"),
        description = "Une application de la DREAL des Pays de la Loire",
        accessibilite = "non"
      ),

      navbarPanel_dsfr(
        title = "Eaux superficielles",
        mod_selection_tableau_bord_ESU_ui("selection_tableau_bord_ESU_1"),
        mod_visualisation_tableau_bord_ESU_ui("visualisation_tableau_bord_ESU_1")
      ),

      navbarPanel_dsfr(
        title = "Eaux souterraines",
        tags$br(),
        tags$h4("La page est en cours de construction."),
        tags$br(),
      ),

      navbarPanel_dsfr(
        title = "Explorer les mesures",
        mod_selection_explorer_mesures_ui("selection_explorer_mesures_1"),
        mod_visualisation_explorer_mesures_ui("visualisation_explorer_mesures_1")

      ),

      navbarPanel_dsfr(
        title = "Consulter les mol\u00e9cules",
        mod_consulter_molecules_ui("consulter_molecules_1")
      ),

      navbarPanel_dsfr(
        title = "A propos",
        tabsetPanel_dsfr(
          id = "a_propos",
          tabPanel_dsfr(
            id = "application",
            title = "L\'application",
            content = tagList(
              shiny::includeMarkdown("inst/app/www/a_propos_application.md")
            )
          ),
          tabPanel_dsfr(
            id = "donnee",
            title = "La donn\u00e9e",
            content = tagList(
              shiny::includeMarkdown("inst/app/www/a_propos_ladonnee.md")
            )
          ),
          tabPanel_dsfr(
            id = "stations_mesure",
            title = "Les stations de mesure",
            content = tagList(
              shiny::includeMarkdown("inst/app/www/a_propos_lesstationsdemesures.md")
            )
          ),
          tabPanel_dsfr(
            id = "classification_mesures",
            title = "La classification des mesures",
            content = tagList(
              shiny::includeMarkdown("inst/app/www/a_propos_laclassificationdesmesures.md")
            )
          ),
          tabPanel_dsfr(
            id = "taux_quantification",
            title = "Le taux de quantification",
            content = tagList(
              shiny::includeMarkdown("inst/app/www/a_propos_taux_quantification.md")
            )
          ),
          tabPanel_dsfr(
            id = "classe_qualite du SeqEau",
            title = "Les classes de qualit\u00e9 du SeqEau",
            content = tagList(
              # shiny::htmlOutput("a_propos_classe_qualite")
              shiny::includeMarkdown("inst/app/www/a_propos_classe_qualite.md")
            )
          ),
          tabPanel_dsfr(
            id = "tableau_classe_qualite",
            title = "Le tableau des classes de qualit\u00e9 du SeqEau",
            shiny::includeHTML("inst/app/www/tableau_classe_seq_eau_molecules.html")
          )
        )
      ),

      navbarPanel_dsfr(
        title = "Cr\u00e9dits et mentions l\u00e9gales",
        shiny::includeMarkdown("inst/app/www/mentionlegale.md")
      ),

      navbarPanel_dsfr(
        title = "Journal",
        shiny::includeMarkdown("inst/app/www/News.md")
      ),
      navbarPanel_dsfr(
        title = "Nous contacter",
        shiny::includeMarkdown("inst/app/www/contacter_nous.md")
      )



    )
  )
}

#' Add external Resources to the Application
#'
#' This function is internally used to add external
#' resources inside the Shiny application.
#'
#' @import shiny
#' @importFrom golem add_resource_path activate_js favicon bundle_resources
#' @noRd
golem_add_external_resources <- function() {
  add_resource_path(
    "www",
    app_sys("app/www")
  )

  tags$head(
    favicon(),
    bundle_resources(
      path = app_sys("app/www"),
      app_title = "pesticides.pdl"
    )
    # Add here other external resources
    # for example, you can add shinyalert::useShinyalert()
  )
}
