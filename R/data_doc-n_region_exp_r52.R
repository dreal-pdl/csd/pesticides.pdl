#' n_region_exp_r52
#'
#' geodataframe de la region des Pays de la Loire.
#'
#' @format A data frame with 1 rows and 4 variables:
#' \describe{
  #'   \item{ id }{  character }
  #'   \item{ nom_reg }{  character }
  #'   \item{ insee_reg }{  character }
  #'   \item{ the_geom }{  sfc_MULTIPOLYGON,sfc }
  #' }
  #' @source DREAL
  "n_region_exp_r52"
