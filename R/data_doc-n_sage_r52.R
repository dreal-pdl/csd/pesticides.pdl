#' n_sage_r52
#'
#' geodataframe des SAGE de la region des Pays de la Loire.
#'
#' @format A data frame with 24 rows and 16 variables:
#' \describe{
  #'   \item{ nid }{  character }
  #'   \item{ vid }{  character }
  #'   \item{ nom }{  character }
  #'   \item{ type }{  character }
  #'   \item{ code }{  character }
  #'   \item{ cd_etat }{  character }
  #'   \item{ lb_etat }{  character }
  #'   \item{ cd_ss_etat }{  character }
  #'   \item{ lb_ss_etat }{  character }
  #'   \item{ cd_comite }{  character }
  #'   \item{ lb_comite }{  character }
  #'   \item{ necessaire_2010 }{  character }
  #'   \item{ necessaire_2016 }{  character }
  #'   \item{ cd_type_pe }{  character }
  #'   \item{ lb_type_pe }{  character }
  #'   \item{ the_geom }{  sfc_GEOMETRY,sfc }
  #' }
  #' @source DREAL
  "n_sage_r52"
